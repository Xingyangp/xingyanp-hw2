from mininet.log import setLogLevel, info
from mininet.link import Link, Intf

def aggNet():
        CONTROLLER_IP="127.0.0.1"
        net = Mininet(topo=None,build=False)
        #add controller
        c0 = net.addController('c0',controller=RemoteController,ip=CONTROLLER_IP)
        #add switch
        switch =net.addSwitch('s1')
        #add host and links
        for h in range(3):
                host=net.addHost('h%s' % (h+1),ip='0.0.0.0')
                net.addLink(host,switch)
        net.start()
        CLI( net )
        net.stop()

if __name__ == '__main__':
        setLogLevel( 'info' )
        aggNet()